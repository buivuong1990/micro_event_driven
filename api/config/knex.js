var path = require("path");

var knex = require("knex")({
    client: 'mysql',
    connection: {
        host: '127.0.0.1',
        user: 'root',
        password: '123',
        database: 'swapez'
    },
    debug: false,
    log: {
        warn(message){
            console.log('warning: ', message);
        },
        error(message){
            console.log('error: ', message);
        },
        deprecate(message) {
            console.log('deprecate: ',message);
        },
        debug(message) {
            console.log('debug: ',message);
        }
    }
});

module.exports = knex;