BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `users` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`email`	varchar ( 255 ),
	`password`	varchar ( 255 )
);
INSERT INTO `users` VALUES (1,'buyer@swap-ez.com','swapez');
INSERT INTO `users` VALUES (2,'saler@swap-ez.com','swapez');
CREATE TABLE IF NOT EXISTS `rams` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar ( 255 )
);
INSERT INTO `rams` VALUES (1,'2GB');
INSERT INTO `rams` VALUES (2,'4GB');
INSERT INTO `rams` VALUES (3,'8GB');
INSERT INTO `rams` VALUES (4,'16GB');
INSERT INTO `rams` VALUES (5,'32GB');
CREATE TABLE IF NOT EXISTS `models` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar ( 255 ),
	`category_id`	integer,
	`brand_id`	integer,
	`color_id`	integer,
	`capacity_id`	integer,
	`ram_id`	INTEGER
);
INSERT INTO `models` VALUES (1,'IPhone X',1,1,2,3,4);
INSERT INTO `models` VALUES (2,'IPhone 7',1,1,1,4,3);
INSERT INTO `models` VALUES (3,'IPad Pro 2016',2,1,2,4,2);
INSERT INTO `models` VALUES (4,'IPad Pro 2018',2,1,2,4,1);
INSERT INTO `models` VALUES (5,'Apple Watch Series 4',3,1,1,2,1);
INSERT INTO `models` VALUES (6,'Samsung Galaxy Note 9',1,2,2,3,4);
INSERT INTO `models` VALUES (7,'Samsung Galaxy S8',1,2,1,3,4);
INSERT INTO `models` VALUES (8,'Samsung Galaxy Tab E T561',2,2,2,4,4);
INSERT INTO `models` VALUES (9,'Samsung Galaxy Tab A 2018',2,2,1,3,4);
INSERT INTO `models` VALUES (10,'Samsung Gear S3 Frontier',3,2,3,1,4);
INSERT INTO `models` VALUES (11,'Sony Xperia XZ2',1,3,3,4,2);
INSERT INTO `models` VALUES (12,'Sony Xperia Z2 SPG561',2,3,2,3,3);
INSERT INTO `models` VALUES (13,'Sony SO-03E XPERIA Z',2,3,2,4,3);
INSERT INTO `models` VALUES (14,'Smartwatch I5 Plus',3,3,3,1,3);
INSERT INTO `models` VALUES (15,'SmartWatch 3 SWR50',3,3,3,1,3);
INSERT INTO `models` VALUES (16,'Huawei Y6 Prime',1,4,1,2,2);
INSERT INTO `models` VALUES (17,'Huawei Nova 3',1,4,1,4,2);
INSERT INTO `models` VALUES (18,'Huawei Y9 2019',1,4,2,3,1);
INSERT INTO `models` VALUES (19,'Huawei 55020533-RF',3,4,3,2,1);
INSERT INTO `models` VALUES (20,'Huawei Honor Watch S1',3,4,3,2,3);
CREATE TABLE IF NOT EXISTS `knexmig_lock` (
	`index`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`is_locked`	integer
);
INSERT INTO `knexmig_lock` VALUES (3,0);
CREATE TABLE IF NOT EXISTS `knexmig` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar ( 255 ),
	`batch`	integer,
	`migration_time`	datetime
);
INSERT INTO `knexmig` VALUES (1,'20181112091751_create_users.js',1,1541989163924);
INSERT INTO `knexmig` VALUES (2,'20181113124256_create_categories_table.js',2,1542087928324);
INSERT INTO `knexmig` VALUES (4,'20181113124846_create_brands_table.js',3,1542088559229);
INSERT INTO `knexmig` VALUES (5,'20181113130459_create_colors_table.js',4,1542089167207);
INSERT INTO `knexmig` VALUES (6,'20181113130808_create_capacities_table.js',5,1542089333053);
INSERT INTO `knexmig` VALUES (7,'20181113132629_create_models_table.js',6,1542090751857);
INSERT INTO `knexmig` VALUES (8,'20181113141723_create_imeis_table.js',7,1542093681304);
CREATE TABLE IF NOT EXISTS `imeis` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`imei`	varchar ( 255 ),
	`model_id`	integer
);
INSERT INTO `imeis` VALUES (1,'0c613af8-e715-11e8',1);
INSERT INTO `imeis` VALUES (2,'31ee22f4-e715-11e8',1);
INSERT INTO `imeis` VALUES (3,'399bde6a-e715-11e8',1);
INSERT INTO `imeis` VALUES (4,'3fbb3426-e715-11e8',2);
INSERT INTO `imeis` VALUES (5,'450f04ca-e715-11e8',2);
INSERT INTO `imeis` VALUES (6,'4f2d029a-e715-11e8',3);
INSERT INTO `imeis` VALUES (7,'55e625c6-e715-11e8',4);
INSERT INTO `imeis` VALUES (8,'652444d2-e715-11e8',5);
INSERT INTO `imeis` VALUES (9,'6cc2201a-e715-11e8',6);
INSERT INTO `imeis` VALUES (10,'72fdc722-e715-11e8',7);
INSERT INTO `imeis` VALUES (11,'7899f6ba-e715-11e8',8);
INSERT INTO `imeis` VALUES (12,'7f371c14-e715-11e8',9);
INSERT INTO `imeis` VALUES (13,'873468ae-e715-11e8',10);
INSERT INTO `imeis` VALUES (14,'90400c1e-e715-11e8',11);
INSERT INTO `imeis` VALUES (15,'97f6ca88-e715-11e8',12);
INSERT INTO `imeis` VALUES (16,'a0a72420-e715-11e8',12);
INSERT INTO `imeis` VALUES (17,'a6a4e9d4-e715-11e8',13);
INSERT INTO `imeis` VALUES (18,'acf9464a-e715-11e8',14);
INSERT INTO `imeis` VALUES (19,'b3e4017a-e715-11e8',14);
INSERT INTO `imeis` VALUES (20,'baac3c8e-e715-11e8',15);
CREATE TABLE IF NOT EXISTS `colors` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar ( 255 ),
	`code`	varchar ( 255 )
);
INSERT INTO `colors` VALUES (1,'Gold','#D4AF37');
INSERT INTO `colors` VALUES (2,'Silver','#C0C0C0');
INSERT INTO `colors` VALUES (3,'Black','#000000');
CREATE TABLE IF NOT EXISTS `categories` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar ( 255 )
);
INSERT INTO `categories` VALUES (1,'Smart Phones');
INSERT INTO `categories` VALUES (2,'Tablets');
INSERT INTO `categories` VALUES (3,'Smart Watches');
CREATE TABLE IF NOT EXISTS `capacities` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar ( 255 )
);
INSERT INTO `capacities` VALUES (1,'4GB');
INSERT INTO `capacities` VALUES (2,'8GB');
INSERT INTO `capacities` VALUES (3,'16GB');
INSERT INTO `capacities` VALUES (4,'32GB');
CREATE TABLE IF NOT EXISTS `brands` (
	`id`	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	`name`	varchar ( 255 ),
	`image`	varchar ( 255 )
);
INSERT INTO `brands` VALUES (1,'Apple','http://demo.swap-ez.com:1004/sub-menu/mobile/apple-gray.png');
INSERT INTO `brands` VALUES (2,'Samsung','http://demo.swap-ez.com:1004/sub-menu/mobile/samsung-gray.png');
INSERT INTO `brands` VALUES (3,'Sony','http://demo.swap-ez.com:1004/sub-menu/mobile/sony.png');
INSERT INTO `brands` VALUES (4,'Huawei','http://demo.swap-ez.com:1004/sub-menu/mobile/huawei.png');
COMMIT;
